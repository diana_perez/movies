//
//  CustomSearchBar.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 23/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

class CustomSearchBar: UISearchBar {

    override func draw(_ rect: CGRect) {
        if let searchField = self.getTextFieldInSubviews() {
            
            if let button = self.getButtonFromSearchBar() {
                button.frame.origin.y = 35
                button.isHidden = true
            }
            
            searchField.frame = CGRect(x : 45.0, y : 30.0, width : frame.size.width - 50, height : frame.size.height - 40.0)
            searchField.textColor = UIColor.white
            searchField.backgroundColor = UIColor.whiteBlur
            
            let textFieldInsideSearchBarLabel = searchField.value(forKey: "placeholderLabel") as? UILabel
            textFieldInsideSearchBarLabel?.textColor = UIColor.white
            
            let clearButton = searchField.value(forKey: "clearButton") as! UIButton
            clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
            clearButton.tintColor = UIColor.white
                    
            let glassIconView = searchField.leftView as? UIImageView
            
            glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
            glassIconView?.tintColor = UIColor.white
        }

        super.draw(rect)


    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame
        searchBarStyle = .prominent
        isTranslucent = false
        
        backgroundImage = UIImage()
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func cancelButtonState(on : Bool) {
        if on {
            self.getButtonFromSearchBar()?.isHidden = false
            self.getTextFieldInSubviews()?.frame = CGRect(x : 45.0, y : 30.0, width : frame.size.width - 115, height : frame.size.height - 40.0)
        } else {
            self.getButtonFromSearchBar()?.isHidden = true
            self.getTextFieldInSubviews()?.frame = CGRect(x : 45.0, y : 30.0, width : frame.size.width - 50, height : frame.size.height - 40.0)
        }
    }
    
    
    
    func getTextFieldInSubviews() -> UITextField? {
        
        let searchBarView = subviews[0]
        for i in 0...(searchBarView.subviews.count-1) {
            if searchBarView.subviews[i].isKind(of : UITextField.self) {
                if let textField = (subviews[0]).subviews[i] as? UITextField {
                    return textField
                }
            }
        }
        
        return nil
    }
    
    
    func getButtonFromSearchBar() -> UIButton? {
        
        let searchBarView = subviews[0]
        
        for i in 0...(searchBarView.subviews.count-1) {
            if searchBarView.subviews[i].isKind(of : UIButton.self) {
                if let button = (subviews[0]).subviews[i] as? UIButton {
                    return button
                }
            }
        }
        
        return nil
    }
    
    
   
}

