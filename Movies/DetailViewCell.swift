//
//  DetailViewCell.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 22/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

class DetailViewCell : UITableViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
  
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingIconImageView: UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
    
        self.thumbnailImageView.layer.cornerRadius = self.thumbnailImageView.frame.size.width/2
        self.thumbnailImageView.layer.masksToBounds = true
        
        var image = UIImage(named: "ratingIcon")
        image = image?.maskWith(color: UIColor.init(red: 228/255, green: 187/255, blue: 59/255, alpha: 1.0))
        
        self.ratingIconImageView.image = image
        
        self.movieTitle.textColor = UIColor.mainColor
        self.movieTitle.font = UIFont.boldSystemFont(ofSize: self.movieTitle.font.pointSize)
        self.yearLabel.textColor = UIColor.darkGray
        self.genresLabel.textColor = UIColor.darkGray
        
        self.ratingLabel.textColor = UIColor.mainColor
        self.overviewLabel.textColor = UIColor.darkGray
        
    }
    
    func config( movie : Movie) -> Void {
        
        self.movieTitle.text = movie.title
        self.yearLabel.text = String(describing : movie.year)
        
        MediaManager.sharedInstance.getMovieImageFromTmdb(movieId: String(describing : movie.ids.tmdb), imageSize: PosterSize.w92) { (image) in
            if image != nil {
                self.thumbnailImageView.image = image
            }
            
        }
        
        self.genresLabel.text = movie.genres?.joined(separator: ", ")
        
        self.ratingLabel.text = String(format: "%.1f", movie.rating!)
        self.overviewLabel.text = movie.overview
        self.overviewLabel.sizeToFit()
    }
    
}

