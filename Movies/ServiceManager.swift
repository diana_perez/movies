//
//  CommonService.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 20/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import SwiftyJSON


class ServiceManager {
    
    var traktURL : String = Bundle.main.object(forInfoDictionaryKey: "TraktApiURL") as! String
    let tmdbURL : String = Bundle.main.object(forInfoDictionaryKey: "TmdbApiURL") as! String
    
    func getFromTraktApi(_ path: String, parameters: [URLQueryItem]?, handler: @escaping ResponseHandler){
        
        var request : URLRequest = URLRequest(url: self.prepareApiRequest(self.traktURL, path: path, parameters : parameters))
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(Bundle.main.object(forInfoDictionaryKey: "TraktCliendID") as! String, forHTTPHeaderField: "trakt-api-key")
        request.addValue(Bundle.main.object(forInfoDictionaryKey: "TraktApiVersion") as! String, forHTTPHeaderField: "trakt-api-version")
        
        request.httpMethod = "GET"
        request.timeoutInterval = 15
        
        URLSession.shared.dataTask(with: request) {data, response, err in
            if err == nil {
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    if statusCode == 200 {
                        let json = JSON(data: data!)
                        
                        if json != nil {
                            handler(json, nil)
                        } else {
                            handler(nil, ApiError.dataError)
                        }
                    } else {
                        handler(nil, ApiError.serviceError)
                    }
                } else {
                    handler(nil, ApiError.responseError)
                }
            } else {
                handler(nil, err)
            }
            
        }.resume()
        
    }
    
    func getFromTmdbApi(_ path: String, parameters: [URLQueryItem]?, handler: @escaping ResponseHandler){
        
        var request : URLRequest = URLRequest(url: self.prepareApiRequest(self.tmdbURL, path: path, parameters : parameters))
        request.httpMethod = "GET"
        request.timeoutInterval = 15
        
        URLSession.shared.dataTask(with: request) {data, response, err in
            if err == nil {
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    if statusCode == 200 {
                        let json = JSON(data: data!)
                        
                        handler(json, nil)
                    } else {
                        handler(nil, ApiError.serviceError)
                    }
                } else {
                    handler(nil, ApiError.responseError)
                }
            } else {
                handler(nil, err)
            }
        }.resume()
        
    }
    
    
    private func prepareApiRequest(_ apiUrl : String, path: String, parameters : [URLQueryItem]?) -> URL {
        
        let url : String = apiUrl + path
        
        var urlComponents : URLComponents = URLComponents(string: url)!
        urlComponents.queryItems = parameters
       
        return urlComponents.url!
    }
    
    
        
        
}
