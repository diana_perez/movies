//
//  MediaDownloadHelper.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 21/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

class MediaDownloadHelper {
    
    static func downloadImage(_ link: String, _ handler : ((UIImage) -> Void)?) {
        if let url = URL(string: link) {
            let session = URLSession.shared
            let dataTask = session.dataTask(with: url, completionHandler: { (data, response, error) in
                if let data = data, let image = UIImage(data:data) {
                    DispatchQueue.main.async(execute: {
                        handler!(image)
                    })
                    
                }
            })
            dataTask.resume()
        }
    }
    
}
