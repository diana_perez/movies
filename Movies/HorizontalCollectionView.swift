//
//  HorizontalCollectionView.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 23/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

class HorizontalCollectionView : UICollectionView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var item : [MovieObject]?
    var cellStyle : String
    var mainViewController : MainViewController?
    
    
    init(frame : CGRect, style : String, viewController : MainViewController) {
        self.mainViewController = viewController
        self.cellStyle = style
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        super.init(frame: frame, collectionViewLayout: layout)
        
        
        backgroundColor = UIColor.white
        register(UINib(nibName : style, bundle : nil), forCellWithReuseIdentifier: style)
        //register(BoxOfficeCollectionViewCell.self, forCellWithReuseIdentifier: "BoxOfficeCollectionViewCell")
        dataSource = self
        delegate = self
        
        showsVerticalScrollIndicator = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.item != nil {
            return self.item!.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellStyle, for: indexPath) as! MovieCollectionViewCell
        
        
        collectionViewCell.config(item : item![indexPath.row], indexPath: indexPath)
        
        return collectionViewCell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        controller.movie = self.item?[indexPath.row].movie
        self.mainViewController?.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width : 90.0, height : 160.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15.0, left: 20.0, bottom: 15.0, right: 15.0)
    }
    
    
}
