//
//  SearchManager.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 21/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import SwiftyJSON

class SearchManager {
    
    static let sharedInstance = SearchManager()
    
    func getSearchResult(page : Int, query : String, completionHandler handler: @escaping ([MovieObject]?, Error?) -> Void) {
        
        let movieRequest : MovieRequest = MovieRequest()
        movieRequest.page = page
        movieRequest.limit = 10
        movieRequest.extended = "full"
        
        let serviceManager = ServiceManager()
        
        serviceManager.getFromTraktApi("search/movie", parameters: [URLQueryItem(name : "query", value : query)] + movieRequest.getUrLQuery() + [GenreFilterManager.sharedInstance.getGenreRequest()], handler: { (data: JSON?, error: Error?) -> Void in
            if(error == nil){
                if data != nil {
                    var movies: [MovieObject] = []
                    if let moviesArr = data!.to(type: MovieObject.self) {
                        movies = moviesArr as! [MovieObject]
                    }
                    
                    handler(movies, nil)
                } else {
                    handler(nil, ApiError.dataError)
                }
                
            }else{
                handler(nil, error)
            }
        })
        
        
    }
    
    
   }

