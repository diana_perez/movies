//
//  UIImageExtension.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 24/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func maskWith(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return newImage
    }        
}
