//
//  CustomSearchController.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 23/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit


class CustomSearchController: UISearchController, UISearchBarDelegate {
    
    var customSearchBar: CustomSearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Initialization
    
    init(searchResultsController: UIViewController!, searchBarFrame: CGRect) {
        super.init(searchResultsController: searchResultsController)
        
        self.configureSearchBar(frame: searchBarFrame)
    }
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureSearchBar(frame: CGRect) {
        customSearchBar = CustomSearchBar(frame: frame)
        
        customSearchBar.barTintColor = UIColor.mainColor
        customSearchBar.tintColor = UIColor.white
        customSearchBar.showsCancelButton = true
        customSearchBar.showsBookmarkButton = false
        
        customSearchBar.setImage(UIImage(named : "filtersselected"), for: .bookmark, state: .normal)
        
        customSearchBar.delegate = self
    }
    
    
}

