//
//  ColorPalet.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 23/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    
    static var mainColor : UIColor {
        get {
            return  UIColor(red: 50/255, green: 60/255, blue: 70/255, alpha: 1.0)
        }
    }
    
    static var whiteBlur : UIColor {
        get {
            return  UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2)
        }
    }
    
    static var secondaryColor : UIColor {
        get {
            return  UIColor(red: 252/255, green: 78/255, blue: 83/255, alpha: 1.0)
        }
    }
    
    
}
