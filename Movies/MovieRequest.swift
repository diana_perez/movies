//
//  CommonRequest.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 20/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation

class MovieRequest {
    
    var page : Int?
    var limit : Int?
    var extended : String?
    
    
    func getUrLQuery() -> [URLQueryItem] {
        var array : [URLQueryItem] = [URLQueryItem]()
        if self.page != nil {
            array.append(URLQueryItem(name: "page", value: String(self.page!)))
        }
        
        if self.limit != nil {
            array.append(URLQueryItem(name: "limit", value: String(self.limit!)))
        }
        
        if self.extended != nil {
            array.append(URLQueryItem(name: "extended", value: self.extended!))
        }
        
        return array
        
    }

    
}

