//
//  MovieResponse.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 20/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import SwiftyJSON

class Movie : JSONable {
    var title : String
    var year : Int
    
    
    var ids : Ids
    
    var released : String?
    var rating : Double?
    var trailer : String?
    var runtime : Int?
    var overview : String?
    var homepage : String?
    var tagline : String?
    var genres : [String]?
    var certification : String?
    var votes : Int?
    
    required init(parameter: JSON) {
        self.title = parameter["title"].stringValue
        self.year = parameter["year"].intValue
        
        self.ids = parameter["ids"].to(type: Ids.self) as! Ids
        
        self.released = parameter["released"].stringValue
        self.rating = parameter["rating"].doubleValue
        self.trailer = parameter["trailer"].stringValue
        self.runtime = parameter["runtime"].intValue
        self.overview = parameter["overview"].stringValue
        self.homepage = parameter["homepage"].stringValue
        self.tagline = parameter["tagline"].stringValue
        self.genres = (parameter["genres"].arrayObject as? [String])
        self.certification = parameter["certification"].stringValue
        self.votes = parameter["votes"].intValue
    }
    
    
}

class Ids : JSONable {
    var imdb : String
    var slug : String
    var tmdb : Int
    var trakt : Int
    
    required init(parameter: JSON) {
        self.imdb = parameter["imdb"].stringValue
        self.slug = parameter["slug"].stringValue
        self.tmdb = parameter["tmdb"].intValue
        self.trakt = parameter["trakt"].intValue
    }

}


class Genres : JSONable {
    var name : String
    var slug : String
    
    var checked : Bool = false
    
    required init(parameter: JSON) {
        self.name = parameter["name"].stringValue
        self.slug = parameter["slug"].stringValue
    }
}

class MovieObject : JSONable {
    var movie : Movie
    var revenue : Int
    var score : Double
    var type : String
    var watchers : Int
    
    
    required init(parameter: JSON) {
        self.movie = parameter["movie"].to(type: Movie.self) as! Movie
        self.score = parameter["score"].doubleValue
        self.type = parameter["type"].stringValue
        self.revenue = parameter["revenue"].intValue
        self.watchers = parameter["watchers"].intValue
    }
    
}

