//
//  MovieDetailViewController.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 22/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

class MovieDetailViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var navBarView: UIView!
    
    public var movie : Movie?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNavControls()
        self.setupMovieDetails()
        self.setupDetailsPageView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        super.viewWillAppear(animated)
        
    }
    
    
    func setupDetailsPageView() {
        self.headerView.tableView?.dataSource = self
        self.headerView.tableView?.delegate = self
        self.headerView.tableView?.separatorColor = UIColor.clear
        self.headerView.headerImageViewContentMode = UIViewContentMode.top
        self.headerView.tableView?.allowsSelection = false
        
        self.headerView.reloadScrollingHeader()
    }
    
    
    func setupNavControls() {
        
        
        let backButton : UIButton = UIButton.init(type: .custom)
        
        backButton.frame = CGRect(x : 10, y : 31, width : 22, height : 22)
        backButton.setImage(UIImage.init(named: "backArrow")?.maskWith(color: UIColor.white), for: .normal)
        backButton.addTarget(self, action: #selector(backToProductViewController), for: .touchUpInside)
        
        self.view.addSubview(backButton)
    }
    
    func backToProductViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupMovieDetails() {
        self.headerView.navBarView = self.navBarView
        MediaManager.sharedInstance.getMovieImageFromTmdb(movieId: String(describing : self.movie!.ids.tmdb), imageSize: PosterSize.w500) { (image) in
            if image != nil {
                self.headerView.imageView?.image = image
            }

        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var detailsCell : DetailViewCell? = tableView.dequeueReusableCell(withIdentifier: "DetailViewCell") as? DetailViewCell
        
        
        if detailsCell == nil {
            detailsCell = Bundle.main.loadNibNamed("DetailViewCell", owner: self, options: nil)?[0] as? DetailViewCell
            detailsCell?.selectionStyle = .none
        }
        
        detailsCell?.config(movie: self.movie!)
        
 
        
        return detailsCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    
}


