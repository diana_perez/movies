//
//  MediaManager.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 21/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import SwiftyJSON

public enum PosterSize : String {
    case w92 = "w92"
    case w154 = "w154"
    case w185 = "w185"
    case w342 = "w342"
    case w500 = "w500"
    case w780 = "w780"
    case original = "original"
    
    
}

class MediaManager {
    
    static let sharedInstance = MediaManager()
    let apiKey : String = Bundle.main.object(forInfoDictionaryKey: "TmdbApiKey") as! String
    
    let imageBaseAddress : String = "https://image.tmdb.org/t/p/"
    
    func getMovieImageFromTmdb(movieId : String, imageSize : PosterSize, completionHandler handler: @escaping (UIImage?) -> Void) {
        
        let serviceManager = ServiceManager()
        
        
        serviceManager.getFromTmdbApi("movie/" + movieId + "/images", parameters: [URLQueryItem(name : "api_key", value : apiKey)], handler: { (data: JSON?, error: Error?) -> Void in
            if(error == nil){
                
                if let posterURL = data?["posters"][0] {
                    if posterURL.dictionary?["file_path"]?.stringValue != nil {
                        MediaDownloadHelper.downloadImage(self.imageBaseAddress + imageSize.rawValue + (posterURL.dictionary?["file_path"]?.stringValue)!, { (image) in
                            handler(image)
                        })
                    }
                }
                
                handler(nil)
                
                
            }
            
            handler(nil)
        })
        
    }
    
}
