//
//  GenreFilterManager.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 25/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import SwiftyJSON

class GenreFilterManager {
    
     static let sharedInstance = GenreFilterManager()
    
    var genres : [Genres]?
    
    func getGenresList(completionHandler handler: @escaping (Error?) -> Void) {
        
        if genres == nil {
            let serviceManager = ServiceManager()
            
            serviceManager.getFromTraktApi("genres/movies", parameters: nil, handler: { (data: JSON?, error: Error?) -> Void in
                if(error == nil){
                    if data != nil {
                        
                        var genres: [Genres] = []
                        if let genresArr = data!.to(type: Genres.self) {
                            genres = genresArr as! [Genres]
                        }
                        self.genres = genres
                        
                        handler(nil)
                    } else {
                        handler(ApiError.dataError)
                    }
                    
                }else{
                    handler(error)
                }
            })
        } else {
            handler(nil)
        }
        
    }

    
    func getGenreRequest() -> URLQueryItem {
        var genresFilter : [String] = [String]()
        if self.genres != nil {
            for genre in self.genres! {
                if genre.checked {
                    genresFilter.append(genre.slug)
                }
            }
        }
        
        if  genresFilter.count > 0 {
            return URLQueryItem(name: "genres", value: genresFilter.joined(separator: ","))
        }
        
        return URLQueryItem(name: "", value: "")
    }
    
}
