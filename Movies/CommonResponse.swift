//
//  CommonResponse.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 20/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias ResponseHandler = (JSON?, Error?) -> Void

enum ApiError : Error {
    case responseError
    case serviceError
    case dataError
}


