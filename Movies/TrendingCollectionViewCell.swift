//
//  CollectionViewCell.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 22/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import UIKit

class TrendingCollectionViewCell: MovieCollectionViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    @IBOutlet weak var likesIcon: UIImageView!
    @IBOutlet weak var viewersLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var image = UIImage(named: "likesIcon")
        image = image?.maskWith(color: UIColor.secondaryColor)
        
        self.likesIcon.image = image
        self.movieTitleLabel.textColor = UIColor.mainColor

        
        self.movieTitleLabel.textColor = UIColor.mainColor
        
        self.movieTitleLabel.lineBreakMode = .byWordWrapping
        self.movieTitleLabel.numberOfLines = 0
        self.movieTitleLabel.textAlignment = .center
        self.movieTitleLabel.sizeToFit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func config(item : MovieObject, indexPath : IndexPath) {
        MediaManager.sharedInstance.getMovieImageFromTmdb(movieId: String(describing : item.movie.ids.tmdb), imageSize: PosterSize.w154) { (image) in
            DispatchQueue.main.async(){
                self.posterImage.image = image
            }
        }
        
        self.viewersLabel.text = String(describing : item.watchers)
        self.movieTitleLabel.text = item.movie.title

    }
}

