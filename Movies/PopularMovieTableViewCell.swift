//
//  PopularMovieTableViewCell.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 24/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import UIKit



class PopularMovieTableViewCell : UITableViewCell {
    
    
    @IBOutlet weak var posterImageview: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
   
    @IBOutlet weak var ratingIconImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!

    @IBOutlet weak var overviewLabel: UILabel!
  
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        var image = UIImage(named: "ratingIcon")
        image = image?.maskWith(color: UIColor.init(red: 228/255, green: 187/255, blue: 59/255, alpha: 1.0))
        
        self.ratingIconImageView.image = image
        self.posterImageview.image = UIImage(named: "posterplaceholder")
        
        self.titleLabel.textColor = UIColor.mainColor
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: self.titleLabel.font.pointSize)
        self.yearLabel.textColor = UIColor.darkGray
        self.genresLabel.textColor = UIColor.darkGray
        
        self.ratingLabel.textColor = UIColor.mainColor
        
        self.overviewLabel.lineBreakMode = .byTruncatingTail
        self.overviewLabel.numberOfLines = 0
        self.overviewLabel.textColor = UIColor.darkGray

    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
  
    
    func config(item : Movie) {
        MediaManager.sharedInstance.getMovieImageFromTmdb(movieId: String(describing : item.ids.tmdb), imageSize: PosterSize.w154) { (image) in
            if image != nil {
                DispatchQueue.main.async(){
                    self.posterImageview.image = image
                }
            }
        }
        
        self.titleLabel.text = item.title
        self.yearLabel.text = String(describing : item.year)
        self.genresLabel.text = item.genres?.joined(separator: ", ")
    
        self.ratingLabel.text = String(format: "%.1f", item.rating!)
        self.overviewLabel.text = item.overview
        
    }
    
    
    override func prepareForReuse() {
        self.titleLabel.text = nil
        self.yearLabel.text = nil
        self.ratingLabel.text = nil
        self.overviewLabel.text = nil
        
        self.posterImageview.image = UIImage(named: "posterplaceholder")
    }
}

