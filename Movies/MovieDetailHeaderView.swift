//
//  MovieDetailHeaderView.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 22/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

class HeaderView : UIView, UIScrollViewDelegate {
    
    static let kDefaultImagePagerHeight : CGFloat = 500.0
    static let kDefaultTableViewHeaderMargin : CGFloat = 20
    static let kDefaultImageScalingFactor : CGFloat = 400.0
    
    public var headerImageViewHeight : CGFloat?
    public var headerImageViewScalingFactor : CGFloat?
    public var navBarViewFadingOffset : CGFloat?
    public var tableView : UITableView?
    public var navBarView : UIView?
    public var headerImageViewContentMode : UIViewContentMode?
    
    
    var imageView : UIImageView?
    var imageButton : UIButton?
    
    private var myContext = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    public func reloadScrollingHeader() {
        self.tableView?.reloadData()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.navBarViewFadingOffset = self.headerImageViewHeight! - (self.navBarView!.frame.height + HeaderView.kDefaultTableViewHeaderMargin)
        if self.tableView == nil {
            self.setupTableView()
        }
        
        if self.tableView?.tableHeaderView == nil {
            self.setupTableViewHeader()
        }
        
        if self.imageView == nil {
            self.setupImageView()
        }
        
        self.setupBackgroundColor()
    }
    
    
    func setupTableView() {
        //TableView
        self.tableView = UITableView.init(frame: self.bounds)
        self.tableView?.backgroundColor  = UIColor.clear
        self.tableView?.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        
        self.tableView?.addObserver(self, forKeyPath: "contentOffset", options: .new, context: &myContext)
        
        self.addSubview(self.tableView!)
    }
    
    func setupTableViewHeader() {
        //Table View Header
        let tableHeaderViewFrame : CGRect = CGRect(x: 0, y: 0, width: self.tableView!.frame.size.width, height: (self.headerImageViewHeight! - HeaderView.kDefaultTableViewHeaderMargin))
        let tableHeaderView : UIView = UIView.init(frame: tableHeaderViewFrame)
        tableHeaderView.backgroundColor = UIColor.clear
        self.tableView?.tableHeaderView = tableHeaderView
    }
    
    
    func setupImageView() {
        //Image View
        self.imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.tableView!.frame.size.width, height: self.headerImageViewHeight!))
        self.imageView?.backgroundColor = UIColor.black
        self.imageView?.autoresizingMask = .flexibleWidth
        self.imageView?.clipsToBounds = true
        self.imageView?.contentMode = self.headerImageViewContentMode!
        self.insertSubview(self.imageView!, belowSubview: self.tableView!)
        
    }
    
    func setupBackgroundColor() {
        
        self.backgroundColor = UIColor.white
        self.tableView?.backgroundColor = UIColor.clear
    }
    
    
    
    func initialize() {
        
        self.headerImageViewHeight = HeaderView.kDefaultImagePagerHeight
        self.headerImageViewScalingFactor = HeaderView.kDefaultImageScalingFactor
        self.headerImageViewContentMode = .scaleAspectFill
        
        self.setupTableView()
        self.setupTableViewHeader()
        self.setupImageView()
        
        self.autoresizesSubviews = true
        self.tableView?.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleTopMargin]
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleTopMargin]
        
    }
    
    deinit {
        self.tableView?.removeObserver(self, forKeyPath: "contentOffset")
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &myContext {
            if keyPath == "contentOffset" {
                self.scrollViewDidScrollWithOffset(self.tableView!.contentOffset.y)
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    
    func scrollViewDidScrollWithOffset(_ scrollOffset: CGFloat) {
        let scrollViewDragPoint : CGPoint = (self.tableView?.contentOffset)!
        
        if scrollOffset < 0 {
            self.imageView?.transform = CGAffineTransform(scaleX: 1 - (scrollOffset / self.headerImageViewScalingFactor!), y: 1 - (scrollOffset / self.headerImageViewScalingFactor!))
        } else {
            self.imageView?.transform =  CGAffineTransform(scaleX: 1, y: 1)
        }
        
        self.animateNavigationBar(scrollOffset, draggingPoint : scrollViewDragPoint)
    }
    
    
    func animateNavigationBar(_ scrollOffset : CGFloat, draggingPoint : CGPoint) {
        
        if self.navBarViewFadingOffset != nil {
            if scrollOffset > self.navBarViewFadingOffset! && self.navBarView?.alpha == 0 {
                self.navBarView?.alpha = 0
                self.navBarView?.isHidden = false
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.navBarView?.alpha = 1
                })
            }
        }
    }
    
}
