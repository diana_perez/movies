//
//  ViewController.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 20/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import UIKit

class MainViewController : UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate  {

    let collectionViewCellWidth = 120
    let collectionViewCellSpace = 10
    var customSearchController: CustomSearchController!
    
    @IBOutlet weak var tableView: UITableView!
    
    var boxOfficeItems : [MovieObject]?
    var trendingItems : [MovieObject]?
    var popularItems : [Movie] = [Movie]()
    
    var filteredMovies : [MovieObject] = [MovieObject]()
    var isFiltering : Bool = false
    var searchText : String?
    var searchPaginationIndex : Int = 1
    
    var paginationIndex : Int = 1
    var isNewdataLoading : Bool = false
    
    var genderFilter : [String]?
    
    var spinner : UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.edgesForExtendedLayout = []
        
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20))
        topView.backgroundColor = UIColor.mainColor
        self.view.addSubview(topView)
    
        tableView.backgroundColor = UIColor.white
        //tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        
        
        //self.spinner.stopAnimating()
        self.spinner.hidesWhenStopped = true
        self.spinner.frame = CGRect(x: 0, y: 40, width: 20, height: 20)
        tableView.tableFooterView = self.spinner
        
        self.setupSearchBar()
        
        MoviesManager.sharedInstance.getBoxOfficeMovies { (moviesArray, error) in
            if error == nil {
                self.boxOfficeItems = moviesArray
                DispatchQueue.main.async(){
                    self.tableView.reloadData()
                    
                }
            } else {
                //Handler error
            }
        }
        
        MoviesManager.sharedInstance.getTrendingMovies { (moviesArray, error) in
            if error == nil {
                self.trendingItems = moviesArray
                DispatchQueue.main.async(){
                    self.tableView.reloadData()
                    
                }
            } else {
                //Handler error
            }
        }
        
        self.paginationIndex = 1
        self.reloadPopularData()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if GenreFilterManager.sharedInstance.genres != nil {
            if (GenreFilterManager.sharedInstance.genres?.count)! > 0 {
                self.popularItems = [Movie]()
                self.paginationIndex = 1
                self.reloadPopularData()
            }
        }
    }
    
    
    func reloadPopularData() {
        
        self.isNewdataLoading = true
        MoviesManager.sharedInstance.getPopularMovies(page: self.paginationIndex) { (moviesArray, error) in
            if error == nil {
                self.isNewdataLoading = false
                if (moviesArray?.count)! > 0 {
                    
                    self.paginationIndex += 1
                    
                    DispatchQueue.main.async(){
                        self.popularItems += moviesArray!
                        self.tableView.reloadData()
                        
                    }
                } else {
                    self.spinner.stopAnimating()
                }
                
            } else {
                //Handler error
            }
        }

        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            if scrollView.contentOffset.y < 0 {
                scrollView.contentOffset = .zero
            }
            
         
            
        }
    }
    

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupSearchBar() {
        
        // Initialize and perform a minimum configuration to the search controller.
        self.customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x : 0.0, y : 0.0, width : tableView.frame.size.width, height : 80.0))
        
        self.customSearchController.customSearchBar.placeholder = "Search ..."
        self.customSearchController.customSearchBar.delegate = self
        
        let filterButton : UIButton = UIButton(type: .custom)
        filterButton.setImage(UIImage(named : "filtersselected"), for: .normal)
        filterButton.sizeToFit()
        filterButton.frame.origin = CGPoint(x: 5, y: 32)
        filterButton.addTarget(self, action: #selector(MainViewController.filterButtonPressed), for: .touchUpInside)

        self.customSearchController.customSearchBar.addSubview(filterButton)
        
        tableView.tableHeaderView = customSearchController.customSearchBar
    
        
    }
    
    func filterButtonPressed() {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GenresFilterViewController") as! GenresFilterViewController
        self.present(controller, animated: true, completion: nil)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !self.isFiltering {
            if section == 0 {
                return "Box Office"
            } else if section == 1 {
                return "Trending"
            } else {
                return "Popular"
            }
        } else {
            return ""
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !self.isFiltering {
            if indexPath.section == 0 || indexPath.section == 1 {
                return 170.0
            } else {
                return 150.0
            }
        } else {
            return 150.0
        }
        
        
    }
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        if !self.isFiltering {
            return 3
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.isFiltering {
            if section == 0 {
                return 1
            } else if section == 1 {
               return 1            } else if section == 2 {
                if self.trendingItems != nil {
                    return self.popularItems.count
                    
                } else {
                    return 0
                }
            }
        
            return 0
        } else {
            return self.filteredMovies.count
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.mainColor
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        header.textLabel?.frame = header.frame
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !self.isFiltering {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "horizontalCollectionViewCell")
        
                let collectionView = HorizontalCollectionView.init(frame: CGRect(x : 0, y : 0, width : Int(cell!.frame.size.width), height : Int(cell!.frame.size.height)), style : "BoxOfficeCollectionViewCell", viewController : self)
                collectionView.item = self.boxOfficeItems
                
            
                cell?.addSubview(collectionView)
            
                return cell!
            } else if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "horizontalCollectionViewCell")
                
                let collectionView = HorizontalCollectionView.init(frame: CGRect(x : 0, y : 0, width : Int(cell!.frame.size.width), height : Int(cell!.frame.size.height)), style : "TrendingCollectionViewCell", viewController : self)
                collectionView.item = self.trendingItems
                
                cell?.addSubview(collectionView)
                
                return cell!
                
            } else {
                var cell = tableView.dequeueReusableCell(withIdentifier: "PopularMovieTableViewCell") as? PopularMovieTableViewCell
                
                if cell == nil {
                    cell = Bundle.main.loadNibNamed("PopularMovieTableViewCell", owner: self, options: nil)?[0] as? PopularMovieTableViewCell
                }

                cell?.config(item: self.popularItems[indexPath.row])
                return cell!
                
                
            }
        } else {
            var cell = tableView.dequeueReusableCell(withIdentifier: "PopularMovieTableViewCell") as? PopularMovieTableViewCell
            
            
            if cell == nil {
                cell = Bundle.main.loadNibNamed("PopularMovieTableViewCell", owner: self, options: nil)?[0] as? PopularMovieTableViewCell
            }
            
            
            cell?.config(item: self.filteredMovies[indexPath.row].movie)
            
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isFiltering {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
            controller.movie = self.filteredMovies[indexPath.row].movie
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            if indexPath.section == 2 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
                controller.movie = self.popularItems[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    // MARK: - UIScrollViewDelegate Delegate
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView.isKind(of : UITableView.self) {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !self.isNewdataLoading {
                    self.spinner.startAnimating()
                    if !self.isFiltering {
                        self.reloadPopularData()
                    } else {
                        self.reloadSearchData()
                    }
                }
            }
        }
    }
    

    
    
    // MARK: - UISearchBarDelegate Delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.customSearchController.customSearchBar.cancelButtonState(on: true)
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.customSearchController.customSearchBar.resignFirstResponder()
        self.customSearchController.customSearchBar.cancelButtonState(on: false)
        
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.customSearchController.customSearchBar.resignFirstResponder()
        self.customSearchController.customSearchBar.cancelButtonState(on: false)
        self.isFiltering = false
        self.searchText = nil
        self.customSearchController.customSearchBar.text = ""
        DispatchQueue.main.async(){
            self.tableView.reloadData()
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count > 0 {
            self.isFiltering = true
            self.searchText = searchText
            self.searchPaginationIndex = 1
            self.spinner.startAnimating()
            SearchManager.sharedInstance.getSearchResult(page : self.searchPaginationIndex, query: searchText, completionHandler: { (movieresults, error) in
                if error == nil {
                    
                    self.searchPaginationIndex += 1
                    DispatchQueue.main.async(){
                        self.filteredMovies = movieresults!
                        self.tableView.reloadData()
                    }
                }
                
                self.spinner.stopAnimating()
            })
            
        } else {
            self.searchText = nil
            self.isFiltering = false
            self.paginationIndex = 1
            DispatchQueue.main.async(){
                self.tableView.reloadData()
            }

        }
    }
    
    func reloadSearchData() {
        self.isNewdataLoading = true
        if searchText != nil {
            SearchManager.sharedInstance.getSearchResult(page : self.searchPaginationIndex, query: self.searchText!, completionHandler: { (movieresults, error) in
                if error == nil {
                    self.isNewdataLoading = false
                    if (movieresults?.count)! > 0 {
                        self.filteredMovies += movieresults!
                        self.searchPaginationIndex += 1
                        DispatchQueue.main.async(){
                            self.tableView.reloadData()
                        }
                    } else {
                        self.spinner.stopAnimating()
                    }
                } else {
                    //handler error
                }
            })
        }
    }

}

