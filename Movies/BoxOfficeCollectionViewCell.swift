//
//  CollectionViewCell.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 22/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    override init(frame : CGRect) {
        super.init(frame : frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func config(item : MovieObject, indexPath : IndexPath) {
        
    }
}

class BoxOfficeCollectionViewCell : MovieCollectionViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var boxOfficeRankLabel: UILabel!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    @IBOutlet weak var boxofficeIcon: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.shadowRadius = 20
        self.layer.shadowOpacity = 0.4
        
        //self.clipsToBounds = false
        var image = UIImage(named: "boxofficeIcon")
        image = image?.maskWith(color: UIColor.secondaryColor)
        
        self.boxofficeIcon.image = image
        self.movieTitleLabel.textColor = UIColor.mainColor
        
        self.movieTitleLabel.lineBreakMode = .byWordWrapping
        self.movieTitleLabel.numberOfLines = 0
        self.movieTitleLabel.textAlignment = .center
        self.movieTitleLabel.sizeToFit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func config(item : MovieObject, indexPath : IndexPath) {
        MediaManager.sharedInstance.getMovieImageFromTmdb(movieId: String(describing : item.movie.ids.tmdb), imageSize: PosterSize.w154) { (image) in
            DispatchQueue.main.async(){
                self.posterImage.image = image
            }
        }
        
        self.boxOfficeRankLabel.text = String(describing : indexPath.row+1)
        self.movieTitleLabel.text = item.movie.title
        

    }
}

