//
//  MoviewManager.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 20/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import SwiftyJSON

class MoviesManager {
    
    static let sharedInstance = MoviesManager()
    static let trackApiUrl = Bundle.main.object(forInfoDictionaryKey: "TraktApiURL") as! String
    
    func getPopularMovies(page : Int, completionHandler handler:  @escaping ([Movie]?, Error?) -> Void) {
        
        let serviceManager = ServiceManager()
        
        let movieRequest = MovieRequest()
        movieRequest.page = page
        movieRequest.limit = 10
        movieRequest.extended = "full"

            
        serviceManager.getFromTraktApi("movies/popular", parameters: movieRequest.getUrLQuery() + [GenreFilterManager.sharedInstance.getGenreRequest()], handler: { (data: JSON?, error: Error?) -> Void in
            if(error == nil){
                if data != nil {
                    var movies: [Movie] = []
                    if let moviesArr = data!.to(type: Movie.self) {
                        movies = moviesArr as! [Movie]
                    }
                    
                    handler(movies, nil)
                } else {
                    handler(nil, ApiError.dataError)
                }
            }else{
                handler(nil, error)
            }
        })
        
        
    }
    
    func getTrendingMovies(completionHandler handler: @escaping ([MovieObject]?, Error?) -> Void) {
        
        let serviceManager = ServiceManager()
        
        let movieRequest = MovieRequest()
        movieRequest.page = 1
        movieRequest.limit = 10
        movieRequest.extended = "full"
        
        serviceManager.getFromTraktApi("movies/trending", parameters: movieRequest.getUrLQuery(), handler: { (data: JSON?, error: Error?) -> Void in
            if(error == nil){
                
                if data != nil {
                    var movies: [MovieObject] = []
                    if let moviesArr = data!.to(type: MovieObject.self) {
                        movies = moviesArr as! [MovieObject]
                    }
                    
                    handler(movies, nil)
                } else {
                
                    handler(nil, ApiError.dataError)
                }
                
                
            }else{
                handler(nil, error)
            }
        })
        
    }
    
    func getBoxOfficeMovies(completionHandler handler: @escaping ([MovieObject]?, Error?) -> Void) {
        
        let serviceManager = ServiceManager()
        
        let movieRequest = MovieRequest()
        movieRequest.page = 1
        movieRequest.limit = 10
        movieRequest.extended = "full"
        
        serviceManager.getFromTraktApi("movies/boxoffice", parameters: movieRequest.getUrLQuery(), handler: { (data: JSON?, error: Error?) -> Void in
            if(error == nil){
                if data != nil {
                    var movies: [MovieObject] = []
                    if let moviesArr = data!.to(type: MovieObject.self) {
                        movies = moviesArr as! [MovieObject]
                    }
                    
                    handler(movies, nil)
                } else {
                    handler(nil, ApiError.dataError)
                }
                
            }else{
                handler(nil, error)
            }
        })
        
    }
    
    func getMovie(movieIdentifier : String) {
        let serviceManager = ServiceManager()
        
        let movieRequest = MovieRequest()
        movieRequest.extended = "full"
        
        serviceManager.getFromTraktApi("movies/" + movieIdentifier, parameters: movieRequest.getUrLQuery(), handler: { (data: JSON?, error: Error?) -> Void in
            if(error == nil){
                
                var movie: Movie
                if let movieObj = data!.to(type: Movie.self) {
                    movie = movieObj as! Movie
                }
                
                
            }else{
                
            }
        })
        
        
    }
}
