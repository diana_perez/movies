//
//  GenderFilterViewController.swift
//  Movies
//
//  Created by PEREZ AFANADOR Diana Maria on 25/11/2016.
//  Copyright © 2016 PEREZ AFANADOR Diana Maria. All rights reserved.
//

import Foundation
import UIKit

class GenresFilterViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.mainColor
        tableView.backgroundColor = UIColor.mainColor
        tableView.dataSource = self
        tableView.delegate = self
        
        let image = UIImage(named : "cancelIcon")?.maskWith(color: UIColor.white)
        self.closeButton.imageView?.image = image
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GenreFilterManager.sharedInstance.getGenresList { (error) in
            if error == nil {
                DispatchQueue.main.async(){
                    self.tableView.reloadData()
                }
            } else {
                //Handler error
            }
        }
    }
    
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.frame.size = CGSize(width: header.frame.size.width, height: 80)
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        header.textLabel?.frame = header.frame
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if GenreFilterManager.sharedInstance.genres != nil {
            return (GenreFilterManager.sharedInstance.genres?.count)!
        }
        return 0
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "genresCell", for: indexPath)
        
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text = GenreFilterManager.sharedInstance.genres?[indexPath.row].name
        cell.backgroundColor = UIColor.clear
        
        if (GenreFilterManager.sharedInstance.genres?[indexPath.row].checked)! {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let genreObj = GenreFilterManager.sharedInstance.genres?[indexPath.row]
        if (genreObj?.checked)! {
            genreObj?.checked = false
        } else {
            genreObj?.checked = true
        }
        
        tableView.reloadData()
    }
    
    
    
}
